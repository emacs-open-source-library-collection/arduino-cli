;;; arduino-cli.el --- Run arduino cli commands via transient -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Oliver Marks

;; Author: Oliver Marks <oly@digitaloctave.com>
;; URL: https://gitlab.com/olymk2/emacs-pythonic-tests
;; Keywords: tools arduino
;; Version: 0.1
;; Created 10 June 2019
;; Package-Requires: ((emacs "26.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implid warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Magit style popups to run arduino cli commands like upload and compile

;;; Code:

(require 'json)
(require 'transient)


(defvar arduino-cli-cmd "arduino-cli")
(defvar arduino-cli-cmd-args "--format json")
(defvar arduino-cli-board nil)
(defvar arduino-cli-port nil)
(defvar arduino-cli-debug t)

(defun arduino-cli-command 
    (&rest 
     args)
  "Build command based on passed in params." 
  (interactive) 
  (let ((cmd (mapconcat 'identity (list arduino-cli-cmd-args (mapconcat 'identity args " ")) " ")))
       (if arduino-cli-debug
           (message "running ...%s" cmd))
       cmd))


(defun arduino-cli-run
    (&rest args) 
  "Call arduino command with ARGS."
  (let* ((cmd (first args))
         (buffer (get-buffer-create "*arduino-cli*")))
         (switch-to-buffer buffer)
         (comint-mode)
         (if arduino-cli-debug
             (message "running ...%s" cmd))
         (message "%s" (apply #'append (list "arduino-cli" buffer arduino-cli-cmd) args))
         (apply 'start-process (apply #'append (list "arduino-cli" buffer arduino-cli-cmd) args))))


(defun arduino-cli-fetch-boards (json-str) 
  "Return a list of arduino boards."
  (let* ((json-object-type 'hash-table) 
         (json-array-type 'list) 
         (json-key-type 'string) 
         (json (json-read-from-string json-str))) 
    (gethash "boards" json)))


(defun arduino-cli-fetch-ports ()
  "Get list of ports from dev folder."
  (directory-files "/dev/" nil "tty\\(ACM\\|USB\\)[0-9]"))

(defun arduino-cli-filter-boards (boards keys) 
  "Pass a list of keys to fetch from the board."
  (mapcar (lambda (value) 
            (mapcar (lambda (k) 
                      (gethash k value)) keys)) boards))

(defun arduino-cli-fqbn () 
  "Return a list of supported boards."
  (arduino-cli-filter-boards
   (arduino-cli-fetch-boards
    (shell-command-to-string
     (concat arduino-cli-cmd " "
             (arduino-cli-command  "board" "listall"))))
   (list "fqbn")))

(defun arduino-cli-build-cmd (transient command &optional args) 
    "Compile sketch with ARGS"
  (interactive (list (transient-args 'arduino-cli-version-transient)))
  (let* ((cmd (append (list  command) args)))
      (arduino-cli-run cmd)))


(defun arduino-cli-version (&optional args) 
    "Compile sketch with ARGS"
  (interactive (list (transient-args 'arduino-cli-version-transient)))
  (let* ((cmd (append (list  "version") args)))
      (arduino-cli-run cmd)))

;;arduino-cli compile --fqbn esp8266:esp8266:d1 .
;;arduino-cli upload -p /dev/ttyUSB0 --fqbn esp8266:esp8266:d1 .
(defun arduino-cli-compile (&optional args) 
    "Compile sketch with ARGS"
  (interactive (list (transient-args 'arduino-cli-compile-transient)))
  (let* ((cmd (append (list  "compile") args)))
      (message "compile %s" cmd) 
      (arduino-cli-run cmd)))


(defun arduino-cli-upload (&optional args) 
  (interactive (list (transient-args 'arduino-cli-upload-transient)))
  (let* ((cmd (append (list  "upload") args)))
      (message "compile %s" cmd) 
      (arduino-cli-run cmd)))


(defun arduino-cli-monitor-serial ()
  (interactive)
  "Open a new buffer which monitors the serial ports communication."
  (let* ((port (first (alist-get 'arduino-cli-transient:--port transient-history)))
         (buffer (get-buffer-create (concat "*" port "*"))))
    (switch-to-buffer buffer)
    (term-mode)
    (make-serial-process
     :port port
     :speed 9600)))


;; (define-infix-command
;;   arduino-cli-transient:--compile
;;   () 
;;   :description "compile" 
;;   :class 'transient-option 
;;   :allow-empty nil 
;;   :shortarg "-b" 
;;   :argument (concat "board" arduino-cli-board) 
;;   :choices (arduino-cli-fqbn))

(define-infix-command 
  arduino-cli-transient:--fqbn
  ()
  :description "Board Fqbn e.g.: arduino:avr:uno"
  :class 'transient-option 
  :allow-empty nil
  :shortarg "-b"
  :argument (concat "--fqbn=" arduino-cli-board)
  :choices (arduino-cli-fqbn))


(define-infix-command 
  arduino-cli-transient:--port
  ()
  "Select the usb port your uploading to."
  :description "USB port" 
  :class 'transient-option 
  :allow-empty nil 
  :shortarg "-p" 
  :reader (lambda ("-->>" (first ( arduino-cli-fetch-ports))) (list nil))
  :argument (concat  "--port=" arduino-cli-port)
  :choices (arduino-cli-fetch-ports))

;;(transient-infix-read arduino-cli-variable:fqbn)

(define-transient-command 
  arduino-cli-compile-transient
  ()
  "Compile Project"
  [ "Arguments"
    ("-d" "Debug" "--debug") 
    (arduino-cli-transient:--fqbn)] 
  [ "Actions" ("c" "Compile" arduino-cli-compile)])

(define-transient-command 
  arduino-cli-upload-transient
  ()
  :value `(,(concat "--port=" (first ( arduino-cli-fetch-ports))))
  [ "Arguments"
    (arduino-cli-transient:--port ) 
    (arduino-cli-transient:--fqbn)] 
  ["Actions" ("u" "Upload" arduino-cli-upload)]
  ["Transient" ("s" "Save" transient-save)])

;(concat "--port=" (first ( arduino-cli-fetch-ports)))
(define-transient-command 
  arduino-cli-core-transient
  ()
  "Core"
  [ "Arguments" (arduino-cli-transient:--port) 
    (arduino-cli-transient:--fqbn)] 
  [ "Actions"
    ("d" "download" arduino-cli-upload) 
    ("i" "install" arduino-cli-upload) 
    ("l" "list" arduino-cli-upload) 
    ("s" "search" arduino-cli-upload) 
    ("U" "uninstall" arduino-cli-upload) 
    ("I" "update-index" arduino-cli-upload) 
    ("u" "Upgrade" arduino-cli-upload)])

(define-transient-command 
  arduino-cli-sketch-transient
  ()
  "Sketch"
  [ "Arguments" (arduino-cli-transient:--port) 
    (arduino-cli-transient:--fqbn)] 
  [ "Actions" ("c" "Compile" arduino-cli-compile) 
    ("n" "New Sketch" arduino-cli-upload)])

;; Main entry point
(define-transient-command 
  arduino-cli-transient
  ()
  "Arduino CLI"
  [ "Arguments"
    ("-d" "Debug" "--debug") 
    ("-h" "Help" "--help") 
    (arduino-cli-transient:--fqbn)] 
  ["Actions"
    ("c" "Compile" arduino-cli-compile-transient) 
    ("b" "Board" arduino-cli-sketch-transient) 
    ;("c" "Compile" arduino-cli-sketch-transient) 
    ;("c" "config" arduino-cli-sketch-transient) 
    ("m" "core" arduino-cli-sketch-transient) 
    ("l" "Lib" arduino-cli-sketch-transient) 
    ;("s" "Sketch" arduino-cli-sketch-transient) 
    ("v" "Version" arduino-cli-version)
    ("s" "Serial Monitor" arduino-cli-monitor-serial)
    ("u" "Upload" arduino-cli-upload-transient)])

;;https://magit.vc/manual/transient/Modifying-Existing-Transients.html#Modifying-Existing-Transients
;;(arduino-cli-transient:--port)
;;transient-suffix-put prefix loc prop value
;; (transient-infix-read 'arduino-cli-transient:--port)
;; (transient-infix-get 'arduino-cli-transient:--port "s")
;; (transient-get-suffix 'arduino-cli-transient "s")
;;(transient-suffix-put "--fqbn" prefix loc prop  "test")

;; (transient-replace-suffix 'arduino-cli-transient ["Arguments" 'arduino-cli-transient:--fqbn] 'arduino-cli-transient:--fqbn)
;; (key-description 'arduino-cli-transient:--fqbn)
;; (key-description 'arduino-cli-transient)

;; (transient-append-suffix
;;   'arduino-cli-transient
;;   #'arduino-cli-transient:--fqbn
;;   (arduino-cli-transient:--fqbn))

;; (transient-append-suffix
;;   'arduino-cli-transient
;;   "-B"
;;  arduino-cli-transient:--fqbn)

;; (transient-append-suffix
;;   'arduino-cli-transient
;;   "a"
;;   '("w" "wip" arduino-cli-transient:--fqbn))

(defun arduino-cli-set-transient-default (transient param value) 
  (if (string= param (oref (get transient 'transient--suffix) 
                           :argument)) 
      (oset (get transient 'transient--suffix) 
            :argument (concat  param value))))

;; (oset (get 'arduino-cli-transient:--fqbn 'transient--suffix)
;;             :value
;;             (concat "test"))


;; (oset (get 'arduino-cli-transient:--fqbn 'transient--suffix)
;;             :init-value
;;             (concat "test"))



(defun arduino-cli-open ()
  "Open transient but set default if defined in dir locals and currently nil."

  ;; (arduino-cli-set-transient-default
  ;;  'arduino-cli-transient:--fqbn
  ;;  "--fqbn="
  ;;  arduino-cli-board)

  ;; (arduino-cli-set-transient-default
  ;;  'arduino-cli-transient:--port
  ;;  "--port="
  ;;  arduino-cli-port)

  ;; (if (string= "port=" (oref (get 'arduino-cli-transient:--port 'transient--suffix) argument))
  ;;     (oset (get 'arduino-cli-transient:--port 'transient--suffix)
  ;;           argument
  ;;           (concat  "port=" arduino-cli-port)))
  ;; (if (string= "board" (oref (get 'arduino-cli-transient:--fqbn 'transient--suffix) argument))
  ;;     (oset (get 'arduino-cli-transient:--fqbn 'transient--suffix)
  ;;           argument
  ;;           (concat  "board=" arduino-cli-board)))
  (arduino-cli-transient))



;(arduino-cli-open)

;;; (Features)
(provide 'arduino-cli)
;;; arduino-cli.el ends here
